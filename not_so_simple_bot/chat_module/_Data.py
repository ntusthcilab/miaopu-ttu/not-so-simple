import pickle

default_fp = "./src/data.pkl"

class Data:
    def __init__(self, fp=None):
        self.__data = {}

        if fp is None:
            self.filename = default_fp
        else:
            self.filename = fp
        
        with open(self.filename, 'wb') as fh:
            pickle.dump(self.__data, fh)

    def read(self):
        with open(self.filename, 'rb') as fh:
            self.__data = pickle.load(fh)
        return

    def write(self):
        with open(self.filename, 'wb') as fh:
            pickle.dump(self.__data, fh)
        return

    # Setter
    def add_user(self, userid):
        self.read()
        if userid not in self.__data.keys():
            self.__data[userid] = {"cat": "", "where": "", "status": "00"}
            self.write()
            # cat: 選哪類 / where: 哪一區
        return

    def remove_user(self, userid):
        self.read()
        del self.__data[userid] # delete key
        self.write()
        return

    def set_user_cat(self, userid, cat):
        self.read()
        self.__data[userid]["cat"] = cat
        self.write()
        return

    def set_user_where(self, userid, where):
        self.read()
        self.__data[userid]["where"] = where
        self.write()
        return

    def set_user_status(self, userid, status):
        self.read()
        self.__data[userid]["status"] = status
        self.write()
        return

    # Getter
    def get_user(self, userid):
        self.read()
        return self.__data[userid] if userid in self.__data else {}

    def get_all_users(self):
        self.read()
        return self.__data.keys()

    def get_user_cat(self, userid):
        self.read()
        return self.__data[userid]["cat"]

    def get_user_where(self, userid):
        self.read()
        return self.__data[userid]["where"]

    def get_user_status(self, userid):
        self.read()
        return self.__data[userid]["status"]
