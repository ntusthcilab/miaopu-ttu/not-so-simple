from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
    TemplateSendMessage, CarouselTemplate, PostbackEvent,
    StickerMessage, StickerSendMessage, ImagemapSendMessage,
    ImageMessage, ImageSendMessage, QuickReply, QuickReplyButton,
    LocationAction
)

# from src.flex import ques_flex, cat_flex

def postback_process(userid, db, event, line_bot_api):
    ret = []

    if "gg" == event.postback.data:
        db.set_user_where(userid, "gg")
        ret = [TextSendMessage(text="你要我推薦幾間給你參考呢？\n請輸入從 1 ~ 12 的數字")]
        db.set_user_status(userid, "10")

    elif "118" == event.postback.data:
        db.set_user_where(userid, "118")
        ret = [TextSendMessage(text="你要我推薦幾間給你參考呢？\n請輸入從 1 ~ 12 的數字")]
        db.set_user_status(userid, "10")

    elif "xsn" == event.postback.data:
        db.set_user_where(userid, "xsn")
        ret = [TextSendMessage(text="你要我推薦幾間給你參考呢？\n請輸入從 1 ~ 12 的數字")]
        db.set_user_status(userid, "10")

    elif "all" == event.postback.data:
        db.set_user_where(userid, "all")
        ret = [TextSendMessage(text="你要我推薦幾間給你參考呢？\n請輸入從 1 ~ 12 的數字")]
        db.set_user_status(userid, "10")

    return ret
