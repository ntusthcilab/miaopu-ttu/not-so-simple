import os.path
import pandas as pd
import random

def find(cat, where, count, fp=None):
    if fp is None:
        fp = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'src', 'find.csv')

    data = pd.read_csv(fp)
    cat_data = data[data['cat'] == cat]

    if where in ["gg", "xsn", "118"]:
        where_data = cat_data[cat_data['where'] == where]
    else:
        where_data = cat_data

    ret = []
    cnt = where_data.shape[0] if where_data.shape[0] < count else count
    idx = random.sample(range(where_data.shape[0]), cnt)
    for x in idx:
        temp = where_data.iloc[x, :]
        temp = {
                    "name": temp['name'], "img_url": temp['img_url'],
                    "star": temp['star'], "desc": temp['desc'], "map_url": temp['map_url']
               }
        ret.append(temp)

    return ret
