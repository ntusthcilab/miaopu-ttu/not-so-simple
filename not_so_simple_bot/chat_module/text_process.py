import re, time
from linebot.models import (
    TextSendMessage, TemplateSendMessage, CarouselTemplate, StickerSendMessage,
)

from not_so_simple_bot.src.flex import where_flex, carousel_flex
from chat_module.shop import find

def text_process(text, userid, line_bot_api, db):
    print(f"This is user: {userid}, text = {text}")
    if userid not in db.get_all_users():
        db.add_user(userid)

    ret = []

    if db.get_user_status(userid) == "10": # 輸入間數
        if not text.isnumeric():
            ret = TextSendMessage(text = "請輸入數字就好~")
        elif int(text) > 12 or int(text) < 1:
            ret = TextSendMessage(text = "我只能推薦最少 1 家、最多 12 家唷")
        else:
            cnt = int(text)
            suggest_list = find(db.get_user_cat(userid), db.get_user_where(userid), cnt)
            if len(suggest_list) == cnt:
                ret = [carousel_flex(suggest_list)]
            else:
                ret = [carousel_flex(suggest_list)] + [TextSendMessage(text = f"抱歉最多只有這{len(suggest_list)}家店")]
            db.set_user_status(userid, "00")

    else:
        if text == "我要吃的":
            ret = [where_flex()]
            db.set_user_cat(userid, "eat")
        elif text == "我要喝的":
            ret = [where_flex()]
            db.set_user_cat(userid, "drink")
        elif text == "我要衣服":
            ret = [where_flex()]
            db.set_user_cat(userid, "cloth")
        elif text == "我要學習相關":
            ret = [where_flex()]
            db.set_user_cat(userid, "edu")
        elif text == "我要找樂子":
            ret = [where_flex()]
            db.set_user_cat(userid, "fun")
        elif text == "我要設定":
            ret = [where_flex()]
            db.set_user_cat(userid, "setting")

    return ret
