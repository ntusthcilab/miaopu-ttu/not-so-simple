from linebot.models import (
    FlexSendMessage
)

from .img import gold_star, gray_star

def where_flex():
    flex_content = {
        "type": "bubble",
        "body": {
            "type": "box",
            "layout": "vertical",
            "contents": [
                {
                    "type": "text",
                    "text": "你想去哪邊？",
                    "weight": "bold",
                    "size": "xl"
                }
            ]
        },
        "footer": {
            "type": "box",
            "layout": "vertical",
            "spacing": "sm",
            "contents": [
                {
                    "type": "button",
                    "style": "link",
                    "height": "sm",
                    "action": {
                        "type": "postback",
                        "label": "公館",
                        "displayText": "我想去公館一帶",
                        "data": "gg"
                    }
                },
                {
                    "type": "button",
                    "style": "link",
                    "height": "sm",
                    "action": {
                        "type": "postback",
                        "label": "118",
                        "data": "118",
                        "displayText": "我想去118"
                    }
                },
                {
                    "type": "button",
                    "style": "link",
                    "height": "sm",
                    "action": {
                        "type": "postback",
                        "label": "新生南",
                        "data": "xsn",
                        "displayText": "我想去新生南一帶"
                    }
                },
                {
                    "type": "button",
                    "style": "link",
                    "height": "sm",
                    "action": {
                        "type": "postback",
                        "label": "傻瓜才做選擇，我都要",
                        "data": "all",
                        "displayText": "我都想看看"
                    }
                }
            ],
            "flex": 0
        }
    }

    flex_message = FlexSendMessage(alt_text = f"請選擇想去哪", contents = flex_content)
    return flex_message

"""
    INPUT: [
        {"name": name, "img_url": img_url, "star": star, "desc": desc, "map_url": map_url}
    ]
    name, img_url, star, desc, map_url
"""
def carousel_flex(suggest_list):
    flex_content = {
        "type": "carousel",
        "contents": []
    }
    for shop in suggest_list:
        name, img_url, star, desc, map_url = shop["name"], shop["img_url"], shop["star"], shop["desc"], shop["map_url"]
        flex_content["contents"].append(bubble(name, img_url, star, desc, map_url))
    print(f"\n\n{flex_content}\n\n")
    flex_message = FlexSendMessage(alt_text = f"選一家店吧", contents = flex_content)
    return flex_message

def bubble(name, img_url, star, desc, map_url):
    ret = {
        "type": "bubble", "size": "micro",
        "hero": { "type": "image", "url": img_url, "size": "full", "aspectMode": "cover", "aspectRatio": "320:213" },
        "body": {
            "type": "box", "layout": "vertical",
            "contents": [
                {"type": "text", "text": name, "weight": "bold", "size": "sm", "wrap": True},
                {"type": "box", "layout": "baseline", "contents": [] },
                {
                    "type": "box", "layout": "vertical",
                    "contents": [{
                        "type": "box", "layout": "baseline", "spacing": "sm",
                        "contents": [ {"type": "text", "text": desc, "wrap": True, "color": "#8c8c8c", "size": "xs", "flex": 5} ]
                    }]
                },
                {
                    "type": "button", "action": {"type": "uri", "label": "我要去", "uri": map_url},
                    "height": "sm", "style": "primary", "adjustMode": "shrink-to-fit", "gravity": "center"
                }
            ],
            "spacing": "sm", "paddingAll": "13px"
        }
    }
    ret["body"]["contents"][1]["contents"] = star_list(star)
    return ret

def star_list(star):
    gold  = {"type": "icon", "size": "xs", "url": gold_star}
    gray  = {"type": "icon", "size": "xs", "url": gray_star}
    point = {"type": "text", "text": "%.1f" % star, "size": "xs", "color": "#8c8c8c", "margin": "md", "flex": 0}
    return [gold] * star + [gray] * (5 - star) + [point]
