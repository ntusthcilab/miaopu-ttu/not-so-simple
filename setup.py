from distutils.core import setup


setup(
    name='not_so_simple_bot',
    version='0.0.2',
    author='NTUST CCE HCI Lab',
    author_email='dianalab2018@gmail.com',
    packages=[
        'not_so_simple_bot',
        'not_so_simple_bot.chat_module',
        'not_so_simple_bot.src',
    ],
    package_data={
        'not_so_simple_bot': ['src/find.csv'],
    },
    license='LICENSE',
    description='MIAOPU TTU',
    install_requires=[
        "flask",
        "line-bot-sdk",
        "gunicorn",
        "pandas",
    ],
)
